// //////////////////////////////////////////////////////////////////
// leadership.js schema model for the leadership object inside mongoDB
/////////////////////////////////////////////////////////////////////
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// /////////////////////////////////////////////////////
// creation of the schema for the leadership
// /////////////////////////////////////////////////////
var leadershipSchema = new Schema({
    name : {
        type : String,
        required: true,
        unique: true
    },
    image : {
        type: String,
        required: true,
    },
    designation : {
        type: String,
        required: true
    },
    abbr : {
        type: String,
        required: true
    },
    description : {
        type: String,
        required: true
    }
});

// /////////////////////////////////////////////////////
// creation of the model to be added to the DB
// /////////////////////////////////////////////////////
var Leadership = mongoose.model('Leadership', leadershipSchema);

// /////////////////////////////////////////////////////
// exporting the module as Leadership
// /////////////////////////////////////////////////////
module.exports = Leadership;