// //////////////////////////////////////////////////////////
// dishes.js schema models for the dishes
// //////////////////////////////////////////////////////////

var mongoose = require('mongoose');

// /////////////////////////////////////////////////////
// loading type for manage currency type
// /////////////////////////////////////////////////////
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;
var Schema = mongoose.Schema;

// ////////////////////////////////////////////////////////////
// commentschema to be added to the schema dishes pre-existing
// ////////////////////////////////////////////////////////////
var commentSchema = new Schema({
    rating:  {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:  {
        type: String,
        required: true
    },
    author:  {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

// /////////////////////////////////////////////////////
// creation of the schema for the dishes with all field
// /////////////////////////////////////////////////////
var dishSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    image : {
        type: String,
        required: true,
        unique: false
    },
    category : {
        type: String,
        required: true,
        unique: false
    },
    label : {
        type: String,
        required: true,
        unique: false,
        default: ""
    },
    price : {
        type : Currency,
        required: true,
        unique: false
    },
    description: {
        type: String,
        required: true
    },    
    comments:[commentSchema]
}, {
    timestamps: true
});

// /////////////////////////////////////////////////////
// creation of the model to be added to the DB
// /////////////////////////////////////////////////////
var Dishes = mongoose.model('Dish', dishSchema);

// /////////////////////////////////////////////////////
// exporting the module as Dishes
// /////////////////////////////////////////////////////
module.exports = Dishes;