//////////////////////////////////////////////////////////
// favoriteRouter.js 
//////////////////////////////////////////////////////////

var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Verify = require('./verify');

// Including the correct models for favorite
var Favorites = require('../models/favorites');

// Using the favoriteRouter necessario
var favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
    .get(Verify.verifyOrdinaryUser, function(req, res, next) {
        Favorites.find({
                'user': req.decoded._doc._id
            })
            .populate('user favoriteDishes')
            .exec(function(err, favorites) {
                if (err) throw err;
                res.json(favorites);
            });
    })

.delete(Verify.verifyOrdinaryUser, function(req, res, next) {
    Favorites.findOne({
        'user': req.decoded._doc._id
    }, function(err, favorites) {
        if (err) throw err;
        if (favorites != null) {
            favorites.favoriteDishes = [];
            favorites.save(function(err, favorite) {
                if (err) throw err;
                console.log('Updated dishes in favorites!');
                res.json(favorite);

            });
        }
    });
})


.post(Verify.verifyOrdinaryUser, function(req, res, next) {

    //crete a favorites object with user
    var favObj = {
        "user": req.decoded._doc._id,
        "favoriteDishes": [req.body._id]
    };

    //find favorite object for the user , if found update the collection, else create a favorite object for the user
    Favorites.findOne({
            'user': req.decoded._doc._id
        })
        .exec(function(err, favorites) {
            if (err) throw err;
			//if favorites for user are found
            if (favorites != null) {

                for (var i = (favorites.favoriteDishes.length - 1); i >= 0; i--) {
                    if (favorites.favoriteDishes[i] == req.body._id) favorites.favoriteDishes.splice(i);
                }

                favorites.favoriteDishes.push(req.body._id);
                favorites.save(function(err, favorite) {
                    if (err) throw err;
                    console.log('Updated your favorites!');
                    res.json(favorite);

                });
				//if favorites for user are not found
            } else {

                Favorites.create(favObj, function(err, favorite) {

                    if (err) throw err;

                    res.writeHead(200, {
                        'Content-Type': 'text/plain'
                    });

                    res.end('created your favorite');
                });
            };
        });
})

;

favoriteRouter.route('/:dishObjectId')
.delete(Verify.verifyOrdinaryUser, function(req, res, next) {
    Favorites.findOne({
        'user': req.decoded._doc._id
    }, function(err, favorites) {
        if (err) throw err;
        if (favorites != null) {
			//find the dish id and remove it from the collection
             for (var i = (favorites.favoriteDishes.length - 1); i >= 0; i--) {
                    if (favorites.favoriteDishes[i] == req.params.dishObjectId) favorites.favoriteDishes.splice(i);
                }
            favorites.save(function(err, favorite) {
                if (err) throw err;
                console.log('Updated dishes in favorites!');
                res.json(favorite);
            });
        }
    });
}) ;

module.exports = favoriteRouter;