// ///////////////////////////////////////////////////////////
// dishes schema: creation of the schema for dishes object 
// inside a mongoDB db collection this permit to store Dishes
// ///////////////////////////////////////////////////////////

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// creation of the dishSchema variable with the specify the field
// of the object that will be stored inside the MongoDB.
var dishSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Dishes = mongoose.model('Dish', dishSchema);

// make this available to our Node applications
module.exports = Dishes;