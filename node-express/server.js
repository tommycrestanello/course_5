/////////////////////////////////////////////////
// server.js implementation by Crestanello Tommy
/////////////////////////////////////////////////

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');

// Module for dishes router : localhost:3000/dishes
var dishRouter = require('./dishRouter');

// Module for dishes router : localhost:3000/leadership
var leaderRouter = require('./leaderRouter');

// Module for dishes router : localhost:3000/promotions
var promoRouter = require('./promoRouter');

var hostname = 'localhost';
var port = 3000;

var app = express();

//using morgan as logger on server side
app.use(morgan('dev'));

//using of the routers with express
app.use('/dishes',dishRouter);
app.use('/leadership',leaderRouter);
app.use('/promotions',promoRouter);

app.use(express.static(__dirname + '/public'));

//Running of the server
app.listen(port, hostname, function(){
  console.log("Server running at http://"+hostname+":"+port+"/");
});
