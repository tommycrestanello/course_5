var express = require('express');
var openurl = require('openurl');
var morgan = require('morgan'); //morgan for log information on server side

var hostname = 'localhost';

var port = 3000;

var app = express();

app.use(morgan('dev'));

app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
  console.log("Server running at http://"+hostname+":"+port+"/");
});

openurl.open("http://localhost:3000");