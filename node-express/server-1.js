//Including of required module express, openurl, http
var express = require('express'),
    openurl = require("openurl"),
     http = require('http');

var hostname = 'localhost';
var port = 3000;

var app = express();

app.use(function (req, res, next) {
  console.log(req.headers);
    res.writeHead(200, { 'Content-Type': 'text/html' });
  res.end('<html><body><h1>Hello World</h1></body></html>');

});

var server = http.createServer(app);

server.listen(port, hostname, function(){
  console.log("Server running at http://"+hostname+":"+port+"/");
});

//With the inclusion of the node module it's possible to open automatically 
//the page of hello worl at localhost:3000
openurl.open("http://localhost:3000");