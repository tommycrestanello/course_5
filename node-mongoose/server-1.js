// //////////////////////////////////////////
// server-1.js 
// //////////////////////////////////////////
var mongoose = require('mongoose'),
    assert = require('assert');

// using the models for the dishes on the DB conFusion
var Dishes = require('./models/dishes-1');

// Connection URL
var url = 'mongodb://localhost:27017/confusion';
mongoose.connect(url);

// connection to the mongoDB via mongoose
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");

    // create a new dishes inside a database
    var newDish = Dishes({
        name: 'Uthapizza',
        description: 'Test'
    });

    //saving all the dishes inside the database
    newDish.save(function (err) {
        if (err) throw err;
        console.log('Dish created!');

        // get all the dishes inside the database
        Dishes.find({}, function (err, dishes) {
            if (err) throw err;

            // object of all the users
            console.log(dishes);
            db.collection('dishes').drop(function () {
                db.close();
            });
        });
    });
});